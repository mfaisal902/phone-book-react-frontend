import React, {useEffect,useState} from "react";
import AddContactForm from "./components/AddContactForm";
import Contact from "./components/ContactItem";
import Heading from "./components/Heading";
import "./App.css";
import axios from "axios";
import { ExclamationCircleOutlined } from '@ant-design/icons';
import {Modal, Empty, Divider} from 'antd';
import {BASE_URL} from './config';
const { confirm } = Modal;

function App() {

    const [contacts, setContacts] = useState([]);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const [currentContact, setCurrentContact] = useState();
    const showModal = () => {
        setIsModalVisible(true);
    };


    useEffect(() => {
        axios.get(BASE_URL+"getAllContacts").then(r=>{
                console.log("All contacts are : ", r)
                setContacts(r.data)
            }).catch(e =>{
                console.log("Error : ", e)
            })
    },[]);


    const handleDelete = (id) => {

        confirm({
            title: 'Are you sure delete this task?',
            icon: <ExclamationCircleOutlined />,
            content: 'Some descriptions',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',

            onOk() {
                console.log('OK'," - ",id);
                axios.delete(BASE_URL+`deleteContact/${id}`)
                    .then(r=>{
                        setContacts(contacts.filter(item => ((item._id === undefined ? item.id : item._id)) !== id));
                    })
                    .catch( e => {
                        console.log("error ", e);
                    })
            },

            onCancel() {
                console.log('Cancel');
            },
        });
    };

    const handleEdit = (contact) => {
        showModal();
        setCurrentContact(contact);
    };


    return (
        <div className="App">
            <Heading/>
            <AddContactForm contacts={contacts} addContact={setContacts} displayBtn={true}/>

            <Divider  dashed="true" orientation="left">
                List of Contacts
            </Divider>

            <div style={{display:'flex',flexDirection:'row',flexWrap: 'wrap',alignItems:'center', justifyContent:'center' }}>
            {
                contacts.length > 0 ? contacts.map((item,index)=>{
                    return <Contact key={item.id | item._id} contact={contacts[index]} handleDelete={handleDelete} handleEdit={handleEdit} />
                }) : <Empty />
            }
            </div>

            <Modal title="Update Contact" maskClosable={false} visible={isModalVisible} onCancel={() => setIsModalVisible(false)} footer={null}>
                <AddContactForm contacts={contacts} addContact={setContacts} displayBtn={false} currentContact={currentContact} closeModel={setIsModalVisible} />
            </Modal>
        </div>
    );
}

export default App;
