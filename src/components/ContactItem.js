import React from "react";
import 'antd/dist/antd.css';
import 'antd/dist/antd.css';
import {DeleteTwoTone, EditTwoTone} from '@ant-design/icons';
import {Avatar, Card, Divider} from 'antd';

const {Meta} = Card;

export default function Contact({contact, handleDelete, handleEdit}) {

    const CardDetails = ({email, phoneNumber}) => {
        return <div>
            <Divider plain="true" dashed="true" orientation="left" orientationMargin="0">Email</Divider>
            <p>{email}</p>
            <Divider plain="true" dashed="true" orientation="left" orientationMargin="0">Phone Number</Divider>
            <p>{phoneNumber}</p>
        </div>
    }

    return (
        <Card
            style={{
                width: '20%',
                margin: 20
            }}
            actions={[
                <DeleteTwoTone key="delete" onClick={() => handleDelete(contact._id === undefined ? contact.id : contact._id)}/>,
                <EditTwoTone key="edit" onClick={() => handleEdit(contact)}/>,
            ]}>

            <Meta
                avatar={<Avatar src="https://joeschmoe.io/api/v1/random" size={90}/>}
                title={contact.name}
                description={<CardDetails email={contact.email} phoneNumber={contact.phoneNumber}/>}
            />
        </Card>
    )
}
