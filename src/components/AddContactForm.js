import React from "react";
import 'antd/dist/antd.css';
import {
    Form,
    Input,
    Select,
    Button,
} from 'antd';
import axios from "axios";
import {BASE_URL} from '../config';
const { Option } = Select;
export default function AddContactForm({contacts,addContact,displayBtn,currentContact,closeModel}) {

    const [form] = Form.useForm();
    const mWidth = displayBtn ? '60%' : '100%';

    const formItemLayout = {
        layout : "vertical",
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 24,
        },
    };

    const onFinish = (values) => {

        if(displayBtn){
            axios.post(BASE_URL+"addContact",{
                "name": values.name,
                "email": values.email,
                "phoneNumber": (values.prefix + values.phone)
            })
                .then(r=>{
                    console.log("After Adding Response is : ",r.data)
                    addContact([...contacts,r.data]);
                })
                .catch(e =>{
                    console.log("Error : ",e)
                })
        }else{

            let data = {};
            if(currentContact._id === undefined){
                data = {
                    "id": currentContact.id,
                    "name": values.name,
                    "email": values.email,
                    "phoneNumber": (values.prefix + values.phone)
                }
            }else{
                data = {
                    "_id": currentContact._id,
                    "name": values.name,
                    "email": values.email,
                    "phoneNumber": (values.prefix + values.phone)
                }
            }

            axios.put(BASE_URL+'updateContact',data)
                .then(r=>{
                    const itemIndex = contacts.findIndex(x => (x._id === undefined ? x.id : x._id) === (currentContact._id === undefined ? currentContact.id : currentContact._id))

                    const item = contacts[itemIndex]

                    item.name = values.name;

                    item.email = values.email;

                    item.phoneNumber = (values.prefix + values.phone)

                    contacts[itemIndex] = item

                    closeModel(false);

                    addContact(contacts);
                })
                .catch( e => {
                    closeModel(false);
                })
        }

    };

    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0,
            },
            sm: {
                span: 16,
                offset: 8,
            },
        },
    };

    const prefixSelector = (
        <Form.Item name="prefix" noStyle>
            <Select
                style={{
                    width: 70,
                }}
            >
                <Option value="40">+40</Option>
                <Option value="44">+44</Option>
            </Select>
        </Form.Item>
    );


    return (
        <div style={{display:'flex',alignItems:'center',justifyContent:'center'}}>
            <Form
                {...formItemLayout}
                form={form}
                name="addContact"
                onFinish={onFinish}
                initialValues={{
                    prefix:  currentContact ?  currentContact.phoneNumber.slice(0,2) : '40',
                    name:currentContact ? currentContact.name : "",
                    email:currentContact ? currentContact.email : "",
                    phone:currentContact ? currentContact.phoneNumber.slice(2,currentContact.phoneNumber.length) : ""
                }}
                scrollToFirstError
                style={{maxWidth: mWidth , padding:"15px"}}>

                <Form.Item
                    name="email"
                    label="E-mail"
                    rules={[
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your E-mail!',
                        },
                    ]}>
                    <Input />
                </Form.Item>


                <Form.Item
                    name="name"
                    label="Full Name"
                    tooltip="What do you want others to call you?"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your nickname!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item
                    name="phone"
                    label="Phone Number"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your phone number!',
                        },
                    ]}
                >
                    <Input
                        addonBefore={prefixSelector}
                        style={{
                            width: '100%',
                        }}
                    />
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                    <Button block type="primary" htmlType="submit">
                            {displayBtn ? "Add Contact" : "Update Contact"}
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
}
