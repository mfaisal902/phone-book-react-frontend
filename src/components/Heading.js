import React from "react";

export default function Heading() {
    return (
        <div className="title">
            <h1>Phone Book React App</h1>
        </div>
    );
}
